# capistranoの出力をカラーに
require 'capistrano_colors'

# cap deploy時に自動で bundle install が実行される
require "bundler/capistrano"

# sshでログインするユーザ
set :user, "shinyay"
# コマンド実行時にsudoをつけるか
set :use_sudo, true
# デプロイサーバ定義
role :web, "sakura"

# lsコマンド実行タスク
task :list  do
    run "ls"
end


# アプリケーション名
set :application,   "rails_capistrano_test"
# subverion, git, mercurial, cvs, bzrなど
set :scm ,:git
#gitリポジトリ
#set :repository,    "git@bitbucket.org:shinyay/rails_capistrano_sample.git"
set :repository,    "https://shinyay@bitbucket.org/shinyay/rails_capistrano_sample.git"
#gitブランチ名
set :branch, "master"

# どういう方式でデプロイするか
# copy デプロイ元でソースを最新化してからデプロイ先にコピー
# checkout デプロイ先に接続した後、scmに応じたcheckoutコマンドを実行する
set :deploy_via , :remote_cache

# SSH
set :use_sudo, true
set :default_run_options, :pty => true
ssh_options[:forward_agent] = true

set :normalize_asset_timestamps, false
# 過去のデプロイしたフォルダを履歴として保持する数
set :keep_releases, 5




set :rails_env, 'development'
# deploy先ディレクトリ
set :deploy_to,    "/var/www/#{rails_env}/#{application}"


# precompile
load 'deploy/assets'

# cap deploy:setup 後、/var/www/sample の権限変更
namespace :setup do
  task :fix_permissions do
    sudo "chown -R #{user}.#{user} #{deploy_to}"
  end
end
after "deploy:setup", "setup:fix_permissions"
# 再起動後に古い releases を cleanup
after 'deploy:restart', 'deploy:cleanup'

# Unicorn 用の設定
set :unicorn_config, "#{current_path}/config/unicorn.rb"
set :unicorn_bin, 'unicorn'
set :unicorn_pid, '/tmp/unicorn.undersky.co.pid'

# Unicorn 用のデーモン操作タスク
def remote_file_exists?(full_path)
  capture("if [ -e #{full_path} ]; then echo 'true'; fi").strip == 'true'
end

def process_exists?(pid_file)
  capture("ps -p `cat #{pid_file}`; true").strip.split("\n").size == 2
end

namespace :deploy do
  desc 'Start Unicorn'
  task :start, roles: :app, except: {no_release: true} do
    if remote_file_exists? unicorn_pid
      if process_exists? unicorn_pid
        logger.important 'Unicorn is already running!', 'Unicorn'
        next
      else
        run "rm #{unicorn_pid}"
      end
    end

    logger.important 'Starting Unicorn...', 'Unicorn'
    run "cd #{current_path} && bundle exec unicorn -c #{unicorn_config} -E #{rails_env} -D"
  end

  desc 'Stop Unicorn'
  task :stop, :roles => :app, :except => {:no_release => true} do
    if remote_file_exists? unicorn_pid
      if process_exists? unicorn_pid
        logger.important 'Stopping Unicorn...', 'Unicorn'
        run "kill -s QUIT `cat #{unicorn_pid}`"
      else
        run "rm #{unicorn_pid}"
        logger.important 'Unicorn is not running.', 'Unicorn'
      end
    else
      logger.important 'No PIDs found. Check if unicorn is running.', 'Unicorn'
    end
  end

  desc 'Reload Unicorn'
  task :reload, :roles => :app, :except => {:no_release => true} do
    if remote_file_exists? unicorn_pid
      logger.important 'Reloading Unicorn...', 'Unicorn'
      run "kill -s HUP `cat #{unicorn_pid}`"
    else
      logger.important 'No PIDs found. Starting Unicorn...', 'Unicorn'
      run "cd #{current_path} && bundle exec unicorn -c #{unicorn_config} -E #{rails_env} -D"
    end
  end

  desc 'Restart Unicorn'
  task :restart, :roles => :app, :except => {:no_release => true} do
    stop
    start
  end
end
