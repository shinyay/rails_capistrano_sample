set :rails_env, 'development'

set :branch, "master"
set :deploy_via, :remote_cache
set :deploy_to, "/var/www/#{rails_env}/#{application}"

role :web, "sakura"
role :app, "sakura:10022"
role :db,  "sakura:10022", :primary => true

set :deploy_to, '/var/www/capistrano_sample/development/'
